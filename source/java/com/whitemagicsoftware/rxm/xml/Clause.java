package com.whitemagicsoftware.rxm.xml;

import java.nio.CharBuffer;

import com.whitemagicsoftware.rxm.grammar.QueryBaseListener;

/**
 * Common behaviour for various clauses.
 */
public class Clause extends QueryBaseListener implements Buffered {
  private static int INDENT = 2;

  private StringBuilder buffer = createBuffer();

  /**
   * Default constructor.
   */
  public Clause() {
  }

  /**
   * Appends a given string to the buffer.
   *
   * @param s The string to append.
   * @return this to chain the append calls.
   */
  protected Clause append( String s ) {
    getBuffer().append( s );
    return this;
  }

  /**
   * Returns the buffer converted to a string. This is the result of
   * the transformed output and should only be called when the parsing
   * of the <b>rxm</b> source is complete.
   *
   * @return A non-null string, possibly empty.
   */
  public String toString() {
    return getBuffer().toString();
  }

  /**
   * Returns the buffer used for building the transformed output.
   *
   * @return A non-null string builder, possibly empty.
   */
  private StringBuilder getBuffer() {
    return this.buffer;
  }

  /**
   * Answers whether the transformed query should be indented with newlines.
   * 
   * @return true Format the output (by default).
   */
  protected boolean beautify() {
    // Returns the System property value.
    return Boolean.getBoolean( "beautify" );
  }

  /**
   * When beautify is enabled, this returns a newline followed by
   * the default amount ofindentation; when disabled, this returns a single
   * space.
   *
   * @return A newline followed by spaces or a single space.
   */
  protected String getNewlineIndent() {
    return getNewline() + (beautify() ? getDefaultIndent() : " ");
  }

  /**
   * Creates a string of spaces that is spaces spaces long.
   *
   * @param spaces The number of spaces to add to the string.
   * @return A string with 'spaces' number of " " padding.
   */
  protected String getIndent( int spaces ) {
    return CharBuffer.allocate( spaces ).toString().replace( '\0', ' ' );
  }

  /**
   * Returns the system newline separator character sequence.
   *
   * @return CR/LF on Windows, LF on Unix, CR on MacOS, or " " if beautify
   * is set true.
   */
  protected String getNewline() {
    return beautify() ? System.lineSeparator() : " ";
  }

  /**
   * Returns the default amount of spaces.
   *
   * @return "  " (by default)
   */
  protected String getDefaultIndent() {
    return getIndent( getIndentBy() );
  }

  /**
   * Returns the number of spaces to indent by.
   *
   * @return 2 (by default)
   */
  protected int getIndentBy() {
    return INDENT;
  }
}

