package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.grammar.QueryParser;

/**
 * Transforms the first <b><code>table &gt; table</code></b> into a SQL
 * <code>FROM</code> expression.
 */
public class FromClause extends Clause {
  private QueryBuilder builder;

  /**
   * Default constructor.
   *
   * @param builder Required to stop listening to events once the first
   * table is obtained.
   */
  public FromClause( QueryBuilder builder ) {
    this.builder = builder;
  }

  /**
   * Returns true if the from clause is ready for writing.
   *
   * @return true Stop listening for events.
   */
  public boolean complete() {
    return toString().length() > 0;
  }

  @Override
  public void enterTableMap( QueryParser.TableMapContext ctx ) {
    String tableName = ctx.table(0).getText();

    append(
      String.format( "%sFROM%s %s %s%s",
        getNewline(),
        getNewlineIndent(),
        tableName,
        tableName,
        getNewline()
      )
    );

    getBuilder().rescindEvents( this );
  }

  private QueryBuilder getBuilder() {
    return this.builder;
  }
}

