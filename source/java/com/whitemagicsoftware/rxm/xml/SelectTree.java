package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.PeekingIterator;
import com.whitemagicsoftware.rxm.Tree;

import java.nio.CharBuffer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Helps generate a SQL/XML SELECT clause.
 */
public class SelectTree extends Tree<Context> implements Buffered {
  public static final int INDENT = 2;

  /**
   * Constructs a tree capable of transforming itself into a SQL/XML
   * SELECT clause.
   *
   * @param payload The data associated with this tree leaf (must not
   * be null).
   */
  public SelectTree( Context payload ) {
    super( payload );
  }

  public String toString() {
    return toString( this, 0 );
  }

  /**
   * This converts all payload instances in the tree to their transformed
   * equivalent using a depth-first traversal.
   *
   * @param tree The tree to transform.
   * @param depth Indicates number of spaces for indentation.
   *
   * @return The fully transformed text.
   */
  protected String toString( Tree<Context> tree, int depth ) {
    StringBuilder sb = createBuffer();
    String indent = "", newline = "";

    if( beautify() ) {
      indent = getIndent( depth );
      newline = getNewline();
    }

    sb.append( indent ).append( start( tree ) );

    List<Tree<Context>> branches = tree.getBranches();
    PeekingIterator<Tree<Context>> i = createIterator( branches.iterator() );
    Context prevContext = null;

    while( i.hasNext() ) {
      Tree<Context> branch = i.next();
      Context context = branch.getPayload();

      if( context instanceof AttributeMapContext ) {
        Tree<Context> parent = branch.getParent();
        AttributeMapContext ctx = (AttributeMapContext)context;

        ctx.hasPrecedingPayload( hasPrecedingPayload( parent, ctx ) );
        ctx.hasFollowingPayload( hasFollowingPayload( parent, ctx ) );
      }
      else if( context instanceof ColumnMapContext ) {
        if( i.hasNext() ) {
          context.setNextContext( i.peek().getPayload() );
        }

        // Ensure that all the column maps get the previous context. This
        // helps when using the divergent list to calculate how many nested
        // elements are in common.
        context.setPrevContext( prevContext );
      }

      // Recurse for all the branches at this level of the tree.
      sb.append( newline ).append( toString( branch, depth + getIndentBy() ) );
      prevContext = context;
    }

    sb.append( newline ).append( indent );

    return sb.append( stop( tree ) ).toString();
  }

  /**
   * Returns a list of contiguous payloads that are of the same class as
   * the given payload.
   */
  private List<Context> getConsecutiveRules(
    Tree<Context> tree, Context p ) {
    List<Context> result = new ArrayList<Context>();

    for( Tree<Context> branch : tree.getBranches() ) {
      if( branch.isPayloadClass( p.getClass() ) ) {
        result.add( branch.getPayload() );
      }
      else {
        // No more adjacent payloads.
        break;
      }
    }

    return result;
  }

  /**
   * Answers whether the payload is preceded by at least one sibling. The
   * siblings must not be split by any other type of playload.
   *
   * @param tree The tree to scan for payload sequences.
   *
   * @param payload The payload with a class that is used as a reference for
   * finding contiguous payloads.
   *
   * @return true There is at least one sibling of the same payload type
   * that is before the given payload.
   */
  public boolean hasPrecedingPayload( Tree<Context> tree, Context payload ) {
    return getConsecutiveRules( tree, payload ).indexOf( payload ) > 0;
  }

  /**
   * Answers whether the payload is followed by at least one sibling. The
   * siblings must not be split by any other type of playload.
   *
   * @param tree The tree to scan for payload sequences.
   *
   * @param payload The payload with a class that is used as a reference for
   * finding contiguous payloads.
   *
   * @return true There is at least one sibling of the same payload type
   * that is after the given payload.
   */
  public boolean hasFollowingPayload( Tree<Context> tree, Context payload ) {
    List<Context> siblings = getConsecutiveRules( tree, payload );
    int index = siblings.indexOf( payload );

    return index > -1 && (index + 1 < siblings.size());
  }

  /**
   * Returns the transformed syntax for beginning the payload item.
   *
   * @param tree The tree that has a payload that contains a token to
   * transform into a string.
   * @return A non-null string, possibly empty.
   */
  protected String start( Tree<Context> tree ) {
    return start( tree.getPayload() );
  }

  /**
   * Helper method for returning the start string.
   *
   * @param payload The payload that contains a token to transform into
   * a string.
   * @return The start string for the parsed item; possibly empty, never null.
   */
  protected String start( Context payload ) {
    return payload.getStart();
  }

  /**
   * Returns the transformed syntax for ending the payload item.
   *
   * @param tree The tree that has a payload that contains a token to
   * transform into a string.
   * @return A non-null string, possibly empty.
   */
  protected String stop( Tree<Context> tree ) {
    return stop( tree.getPayload() );
  }

  /**
   * Helper method for returning the stop string.
   *
   * @param payload The payload that contains a token to transform into
   * a string.
   * @return The stop string for the parsed item; possibly empty, never null.
   */
  protected String stop( Context payload ) {
    return payload.getStop();
  }


  private PeekingIterator<Tree<Context>> createIterator(
    Iterator<Tree<Context>> iterator ) {
    return new PeekingIterator<Tree<Context>>( iterator );
  }

  protected String getNewline() {
    return System.lineSeparator();
  }

  /**
   * Answers whether the transformed query should be indented with newlines.
   * 
   * @return true Format the output (by default).
   */
  protected boolean beautify() {
    // Returns the System property value.
    return Boolean.getBoolean( "beautify" );
  }

  /**
   * Used by subclasses to beautify the leading SQL clause (e.g., SELECT,
   * FROM, WHERE).
   *
   * @param spaces The number of spaces to include in the result.
   * @return A non-null string with 'spaces' amount of space.
   */
  protected String getSpace( int spaces ) {
    return beautify() ? getNewline() + getIndent( spaces ) : " ";
  }

  /**
   * Helper method to return the default space amount.
   *
   * @return A non-null string with the default amount of space.
   */
  protected String getSpace() {
    return getSpace( getIndentBy() );
  }

  /**
   * Creates a string of spaces that is spaces spaces long.
   *
   * @param spaces The number of spaces to add to the string.
   * @return A string with 'spaces' number of " " padding.
   */
  protected String getIndent( int spaces ) {
    return CharBuffer.allocate( spaces ).toString().replace( '\0', ' ' );
  }

  /**
   * Returns the default amount of spaces.
   *
   * @return "  " (by default)
   */
  protected String getDefaultIndent() {
    return getIndent( getIndentBy() );
  }

  /**
   * Returns the number of spaces to indent by.
   *
   * @return 2 (by default)
   */
  protected int getIndentBy() {
    return INDENT;
  }
}
