package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.grammar.QueryParser;

/**
 * Transforms <b><code>table.column +&gt; table.column</code></b> into a SQL
 * <code>INNER JOIN</code> expression; similarly, transforms the operand
 * <code>-&gt;</code> into <code>OUTER JOIN</code>.
 */
public class JoinClause extends Clause {
  /**
   * Default constructor.
   */
  public JoinClause() {
  }

  @Override
  public void enterJoinMap( QueryParser.JoinMapContext ctx ) {
    QueryParser.TableColumnContext
      lhs = ctx.tableColumn( 0 ),
      rhs = ctx.tableColumn( 1 );

    String lhsTable = lhs.table().getText();

    append( String.format( "%s JOIN %s %s ON%s%s%s = %s%s%s",
      ctx.T_OUTER() == null ? "INNER" : "OUTER",
      lhsTable,
      lhsTable,
      getNewlineIndent(),
      lhsTable,
      lhs.column().getText(),
      rhs.table().getText(),
      rhs.column().getText(),
      getNewline() )
    );
  }
}

