package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.grammar.QueryParser;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Transforms <b><code>table &gt; path</code></b> into an
 * <code>XMLELEMENT</code> expression.
 */
public class TableMapContext extends Context {
  public TableMapContext( ParserRuleContext ctx ) {
    super( ctx );
  }

  /**
   * Opens the XMLELEMENT.
   *
   * @return ",XMLELEMENT(" ...
   */
  @Override
  public String getStart() {
    ParserRuleContext p = getParserRuleContext();
    QueryParser.TableMapContext ctx = (QueryParser.TableMapContext)p;
    String tableName = ctx.table(1).getText();

    return String.format( ",%s", startElement( tableName ) );
  }
}
