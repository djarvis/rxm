package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.RXM;
import com.whitemagicsoftware.rxm.ProxyParseTreeListener;

import com.whitemagicsoftware.rxm.grammar.QueryBaseListener;
import com.whitemagicsoftware.rxm.grammar.QueryLexer;
import com.whitemagicsoftware.rxm.grammar.QueryParser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * Parses the Relational eXpression Map (<b>rxm</b>) into a tree.
 */
public class QueryBuilder extends QueryBaseListener {
  /** Map for containing modules. */
  private RXM rxm;

  private SelectClause selectClause = new SelectClause();
  private FromClause fromClause = new FromClause( this );
  private JoinClause joinClause = new JoinClause();
  private WhereClause whereClause = new WhereClause();

  private ProxyParseTreeListener proxy = new ProxyParseTreeListener();

  /**
   * Creates a QueryBuilder instance that parses a given <b>rxm</b>.
   *
   * @param rxm The <b>rxm</b> instance responsible for receiving information
   * that allows it to build a SQL statement. Must not be null.
   */
  public QueryBuilder( RXM rxm ) {
    this.rxm = rxm;
  }

  /**
   * Parses the given <b>rxm</b> text. This creates a tree that is used
   * for context when building the query.
   *
   * @param rxm The <b>rxm</b> string to parse.
   */
  public void parse( String rxm ) {
    QueryLexer lexer = new QueryLexer( new ANTLRInputStream( rxm ) );
    CommonTokenStream tokens = new CommonTokenStream( lexer );
    QueryParser.QueryContext ctx = (new QueryParser( tokens )).query();

    eventsDeregister();
    eventsRegister();
    eventsNotify( ctx );
  }

  /**
   * Allows reusing the instance.
   */
  private void eventsDeregister() {
    getProxyParseTreeListener().clear();
  }

  /**
   * Registers the initial event listeners: SELECT, FROM, and JOIN. The
   * WHERE clause listener only starts to receive events if the where
   * clause syntax event is triggered (i.e., the WHERE clause terminator is
   * encountered).
   */
  private void eventsRegister() {
    receiveEvents( this );
    receiveEvents( getSelectClause() );
    receiveEvents( getFromClause() );
    receiveEvents( getJoinClause() );
  }

  /**
   * Walks the tree and issues the events.
   */
  private void eventsNotify( QueryParser.QueryContext ctx ) {
    ParseTreeWalker.DEFAULT.walk( getProxyParseTreeListener(), ctx );
  }

  /**
   * Adds the given listener to the list of listeners that receive
   * parser events.
   *
   * @param listener The listener to add to the listener list.
   */
  public void receiveEvents( ParseTreeListener listener ) {
    getProxyParseTreeListener().add( listener );
  }

  /**
   * Removes the given listener from the list of listeners that receive
   * parser events.
   *
   * @param listener The listener to remove from the listener list.
   */
  public void rescindEvents( ParseTreeListener listener ) {
    getProxyParseTreeListener().remove( listener );
  }

  /**
   * The where clause begins after all the statements. The where clause
   * is added to the listeners at this point to receive the enterWhere
   * and subsequent events. Listening at enterWhere would be too late.
   */
  @Override
  public void exitStatements(
    QueryParser.StatementsContext ctx ) {
    receiveEvents( getWhereClause() );
    rescindEvents( getJoinClause() );
    rescindEvents( getSelectClause() );
  }

  /**
   * Invoked when there are no more <b>rxm</b> tokens to parse.
   *
   * @param ctx Indicates that the query has been parsed.
   */
  @Override
  public void exitQuery( QueryParser.QueryContext ctx ) {
    String select = getSelectClause().toString();
    String from = getFromClause().toString();
    String join = getJoinClause().toString();
    String where = getWhereClause().toString();

    System.out.printf( "%s%s%s%s", select, from, join, where );
  }

  /**
   * Returns the Relational eXpression Map to populate while walking
   * through the AST.
   *
   * @return The <b>rxm</b> instance to update with information from
   * the AST.
   */
  private RXM getRXM() {
    return this.rxm;
  }

  /**
   * Returns the SQL SELECT clause expression.
   * 
   * @return The SELECT portion of a SQL statement.
   */
  private SelectClause getSelectClause() {
    return this.selectClause;
  }

  /**
   * Returns the SQL FROM clause expression.
   *
   * @return The FROM portion of a SQL statement.
   */
  private FromClause getFromClause() {
    return this.fromClause;
  }

  /**
   * Returns the tables JOINed together that are used in the SELECT clause
   * of the SQL statement.
   * 
   * @return The INNER/OUTER JOIN portion of a SQL statement.
   */
  private JoinClause getJoinClause() {
    return this.joinClause;
  }

  /**
   * Returns the SQL WHERE clause expressions.
   * 
   * @return The WHERE portion of a SQL statement.
   */
  private WhereClause getWhereClause() {
    return this.whereClause;
  }

  private ProxyParseTreeListener getProxyParseTreeListener() {
    return this.proxy;
  }
}

