package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.DivergentList;
import com.whitemagicsoftware.rxm.grammar.QueryParser;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Transforms <b><code>column &gt; path</code></b> into an
 * <code>XMLELEMENT</code> expression.
 */
public class ColumnMapContext extends Context {
  // The most recent column mapping path that does not contain ellipses.
  private QueryParser.ElementPathContext explicitPath;

  /**
   * Default constructor (calls super).
   * 
   * @param ctx The payload that associates the parser rule context with
   * the abstract syntax tree.
   * @param tableName Name of the table associated with this column.
   */
  public ColumnMapContext( ParserRuleContext ctx, String tableName ) {
    super( ctx, tableName );
  }

  /**
   * Opens the XMLELEMENT.
   *
   * @return ",XMLELEMENT(" ...*
   */
  @Override
  public String getStart() {
    DivergentList<String> thisList = listElementNames();
    DivergentList<String> prevList = listElementNamesPrev();

    // If there are no common elements with the previous list, then this is
    // an entirely new list of elements. This logic is tricky:
    //
    // 1. diverges(...) returns the last matching index or -1 for no matches;
    // 2. the last element name in the path must be unique; therefore,
    // 3. adding 1 produces the starting index into thisList for
    // appending ,XMLELEMENTs.
    //
    // Closing the parentheses is performed in getStop().
    int diverges = thisList.diverges( prevList ) + 1;
    StringBuilder sb = createBuffer();

    for( String element : thisList.subList( diverges, thisList.size() ) ) {
      sb.append( ',' ).append( startElement( element ) );
    }

    sb.append( String.format( ",%s.%s", getTableName(), getColumnText() ) );

    return sb.toString();
  }

  /**
   * Closes the opening XMLELEMENT parentheses.
   *
   * @return ")" times the number of required closing parentheses.
   */
  @Override
  public String getStop() {
    DivergentList<String> thisList = listElementNames();
    DivergentList<String> nextList = listElementNamesNext();

    // Determine the number of closing parentheses by noting how many
    // elements the next column mapping (if any) has in common with the
    // current mapping.
    return repeat( thisList.size() - thisList.diverges( nextList ) - 1, ')' );
  }

  /**
   * Returns a list of element names that can be compared against another
   * list (to ascertain where the lists diverge).
   *
   * @return A non-null, possibly empty, divergent list.
   */
  protected DivergentList<String> listElementNames() {
    DivergentList<String> result = createDivergentList();
    result.addAll( elementNames() );
    return result;
  }

  /**
   * Helper method.
   */
  private DivergentList<String> listElementNames( Context ctx ) {
    return ctx != null && ctx instanceof ColumnMapContext ?
      ((ColumnMapContext)ctx).listElementNames() :
      createDivergentList();
  }

  /**
   * Helper method.
   */
  private DivergentList<String> listElementNamesNext() {
    return listElementNames( getNextContext() );
  }

  /**
   * Helper method.
   */
  private DivergentList<String> listElementNamesPrev() {
    return listElementNames( getPrevContext() );
  }

  /**
   * Returns an insertion-order set of element names.
   */
  private Set<String> elementNames() {
    Set<String> set = new LinkedHashSet<String>();

    element().forEach( e -> set.add( e.getText() ) );

    return set;
  }

  protected boolean hasEllipsesPath() {
    return ellipsesPath() != null;
  }

  /**
   * Returns the list of elements associated with this context. This
   * is used to obtain the list of element names for the divergent lists.
   *
   * @return The list of ElementContext instances that contain element names.
   */
  protected List<QueryParser.ElementContext> element() {
    List<QueryParser.ElementContext> result = (hasEllipsesPath() ?
      ellipsesPath().elementPath() :
      elementPath()).element();

    if( hasEllipsesPath() ) {
      result.addAll( 0, explicitPath() );
    }

    return result;
  }

  private List<QueryParser.ElementContext> explicitPath() {
    List<QueryParser.ElementContext> result =
      new ArrayList<QueryParser.ElementContext>();

    QueryParser.ElementPathContext explicitPath = getExplicitPath();

    if( explicitPath != null ) {
      result.addAll( explicitPath.element() );
      result.remove( result.size() - 1 );
    }

    return result;
  }

  protected void setExplicitPath( QueryParser.ElementPathContext epc ) {
    this.explicitPath = epc;
  }

  private QueryParser.ElementPathContext getExplicitPath() {
    return this.explicitPath;
  }

  /**
   * Helper method.
   *
   * @return The element path containing ElementPathContext instances having
   * element names.
   */
  protected QueryParser.ElementPathContext elementPath() {
    return path().elementPath();
  }

  /**
   * Helper method.
   *
   * @return The element path containing EllipsesPathContext instances having
   * an ellipses prefix and element names.
   */
  protected QueryParser.EllipsesPathContext ellipsesPath() {
    return path().ellipsesPath();
  }

  /**
   * Returns the path from the column map context, rather than using
   * the superclass's table map context.
   *
   * @return The path associated with the column map parser rule.
   */
  protected QueryParser.PathContext path() {
    return getColumnMapContext().path();
  }
}

