package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.grammar.QueryParser;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Transforms <b><code>table &gt; @attribute</code></b> into an
 * <code>XMLATTRIBUTES</code> expression.
 */
public class AttributeMapContext extends Context {
  private boolean hasPrecedingPayload = false;
  private boolean hasFollowingPayload = false;

  public AttributeMapContext( ParserRuleContext ctx, String tableName ) {
    super( ctx, tableName );
  }

  /**
   * Opens the XMLATTRIBUTES expression.
   *
   * @return Token containing ",XMLATTRIBUTES(" ...
   */
  @Override
  public String getStart() {
    String
      tableName = getTableName(),
      columnName = getColumnText(),
      attributeName = getAttributeText();

    return String.format( ",%s", hasPrecedingPayload() ?
      nextAttribute( tableName, columnName, attributeName ) :
      firstAttribute( tableName, columnName, attributeName )
    );
  }

  /**
   * Closes the XMLATTRIBUTES expression, should no more attributes be
   * forthcoming.
   *
   * @return Token containing ")" or the empty string.
   */
  @Override
  public String getStop() {
    return hasFollowingPayload() ? "" : super.getStop();
  }

  private String getAttributeText() {
    return attribute().getChild(1).getText();
  }

  /**
   * Returns the attribute (node) name.
   */
  private QueryParser.AttributeContext attribute() {
    return getAttributeMapContext().attribute();
  }

  /**
   * Returns the attribute map context that this class wraps.
   */
  private QueryParser.AttributeMapContext getAttributeMapContext() {
    return (QueryParser.AttributeMapContext)getParserRuleContext();
  }

  @Override
  protected QueryParser.ColumnContext column() {
    return getAttributeMapContext().column();
  }

  /**
   * Formats the start of the XMLATTRIBUTES call.
   *
   * @param table The table name assigned to the attribute.
   * @param column The column name assigned to the attribute.
   * @param node The document node name assigned to the attribute.
   *
   * @return The text used for SQL/XML XMLATTRIBUTES calls.
   */
  private String firstAttribute( String table, String column, String node ) {
    return "XMLATTRIBUTES(" + nextAttribute( table, column, node );
  }

  /**
   * Formats the next attribute of the XMLATTRIBUTES call. This is only
   * called when there are contiguous attributes listed in the <b>rxm</b>.
   *
   * @param table The table name assigned to the attribute.
   * @param column The column name assigned to the attribute.
   * @param node The document node name assigned to the attribute.
   *
   * @return The text used for SQL/XML XMLATTRIBUTES calls.
   */
  private String nextAttribute( String table, String column, String node ) {
    // If the column name and attribute name are the same, then the "AS"
    // clause is superfluous.
    node = column.equals( node ) ? "" : String.format( " AS \"%s\"", node );

    return String.format( "%s.%s%s", table, column, node );
  }

  public void hasPrecedingPayload( boolean hasPrecedingPayload ) {
    this.hasPrecedingPayload = hasPrecedingPayload;
  }

  public void hasFollowingPayload( boolean hasFollowingPayload ) {
    this.hasFollowingPayload = hasFollowingPayload;
  }

  private boolean hasPrecedingPayload() {
    return this.hasPrecedingPayload;
  }

  private boolean hasFollowingPayload() {
    return this.hasFollowingPayload;
  }
}

