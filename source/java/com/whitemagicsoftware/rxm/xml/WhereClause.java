package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.grammar.QueryParser;

import org.antlr.v4.runtime.tree.TerminalNode;

/**
 */
public class WhereClause extends Clause {
  /**
   * Default constructor.
   */
  public WhereClause() {
  }

  /**
   * Appends "<code>WHERE </code>" to the output.
   *
   * @param ctx The WHERE expression context (unused).
   */
  public void enterWhere( QueryParser.WhereContext ctx ) {
    append( "WHERE" ).append( getNewlineIndent() );
  }

  /**
   * Appends "<code> AND </code>" to the output.
   *
   * @param ctx The AND expression context (unused).
   */
  public void exitExprAnd( QueryParser.ExprAndContext ctx ) {
    append( getNewlineIndent() ).append( "AND " );
  }

  /**
   * Appends "<code> OR </code>" to the output.
   *
   * @param ctx The OR expression context (unused).
   */
  public void exitExprOr( QueryParser.ExprOrContext ctx ) {
    append( getNewlineIndent() ).append( "OR " );
  }

  /**
   * Appends "<code>(</code>" to the output.
   *
   * @param ctx The parenthesis expression context (unused).
   */
  public void enterExprParen( QueryParser.ExprParenContext ctx ) {
    append( "(" );
  }

  /**
   * Appends "<code>)</code>" to the output.
   *
   * @param ctx The parenthesis expression context (unused).
   */
  public void exitExprParen( QueryParser.ExprParenContext ctx ) {
    append( ")" );
  }

  /**
   * Transforms an <b>rxm</b> equality comparator into ANSI SQL.
   * Note: There is probably a cleaner way to implement this monstrosity.
   *
   * @param ctx The equality comparison context.
   */
  public void enterExprCompEqual( QueryParser.ExprCompEqualContext ctx ) {
    QueryParser.TableColumnContext tableColumn = ctx.tableColumn();
    QueryParser.ExprSetContext set = ctx.exprSet();

    // If the terminal node is null, then the equality is T_INEQ.
    TerminalNode equals = ctx.getToken( QueryParser.T_EQ, 0 );

    String entity = tableColumn.getText();
    String equalTo = (equals == null) ? "NOT " : "";

    if( set == null ) {
      // Presume = or <> by default (changes to IS or IS NOT as needed).
      String comparator = ctx.getChild(1).getText();
      QueryParser.LiteralContext literal = ctx.exprValue().literal();

      if( literal == null ) {
        // No literal value to examine means it is probably a parameter.
        append( ctx.getText() );
      }
      else {
        // Default is to presume the value literal is not the 'null' token.
        String rhs = literal.getText();

        // If the "null" literal terminal node is not null, then null has
        // been found and so the value must be set to NULL and the
        // comparators changed from symbols to ANSI SQL keywords.
        if( literal.T_NULL() != null ) {
          rhs = "NULL";
          comparator = " IS " + equalTo;
        }

        append( String.format( "%s%s%s", entity, comparator, rhs ) );
      }
    }
    else {
      // Transform to ANSI SQL set notation.
      append( String.format( "%s %sIN (%s)",
        entity,
        equalTo,
        set.exprList().getText() ) );
    }
  }

  /**
   * Appends a relational expression (less than, greater than, etc.) to the
   * output.
   *
   * @param ctx The relational expression context.
   */
  public void enterExprCompRel( QueryParser.ExprCompRelContext ctx ) {
    append( ctx.getText() );
  }
}
