package com.whitemagicsoftware.rxm.xml;

import java.nio.CharBuffer;

import java.util.List;

import com.whitemagicsoftware.rxm.DivergentList;
import com.whitemagicsoftware.rxm.Tree;
import com.whitemagicsoftware.rxm.grammar.QueryParser;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Represents a wrapper class for ParserRuleContext.
 */
public abstract class Context implements Buffered {
  private ParserRuleContext parserRuleContext;
  private Context nextContext;
  private Context prevContext;

  private String tableName = "";

  /**
   * Constructs a wrapper class that provides the ParserRuleContext and
   * tree for the *Context classes.
   *
   * @param payload The data that contains the ParserRuleContext.
   */
  protected Context( ParserRuleContext payload ) {
    setParserRuleContext( payload );
  }

  /**
   * Constructs a wrapper class that provides the ParserRuleContext and
   * tree for the *Context classes.
   *
   * @param payload The parser rule context to wrap in this context.
   * @param tableName The name of the table with respect to this context.
   */
  protected Context( ParserRuleContext payload, String tableName ) {
    this( payload );
    setTableName( tableName );
  }

  /**
   * Opens the current transformation expression. Subclasses must override
   * this to provide the correct opening SQL/XML expression. It could be
   * made abstract, but is a useful convenience method.
   *
   * @return "("
   */
  public String getStart() {
    return "(";
  }

  /**
   * Closes the current transformation expression. Subclasses may override
   * this to provide a custom closing SQL/XML expression.
   *
   * @return ")"
   */
  public String getStop() {
    return ")";
  }

  /**
   * Formats the start of the XMLELEMENT call. This is used by a number
   * of subclasses to write an XMLELEMENT call.
   * 
   * @param name The name assigned to the element.
   *
   * @return The text used for SQL/XML XMLELEMENT calls.
   */
  protected String startElement( String name ) {
    return String.format( "XMLELEMENT(NAME \"%s\"", name );
  }

  /**
   * Returns an empty list of elements.
   *
   * @return A non-null, empty divergent list.
   */
  protected DivergentList<String> createDivergentList() {
    return new DivergentList<String>();
  }

  /**
   * This will return column name without the preceding period.
   *
   * @return The column name, without a leading period.
   */
  protected String getColumnText() {
    // The first child (index 0) is the leading period.
    // The second child (index 1) is the column name.
    return column().getChild(1).getText();
  }

  /**
   * Changes the name of the parent table.
   *
   * @param tableName The new table name context (can be null).
   * @return The new table name or empty string if 'tableName' is null.
   */
  public String setTableName( String tableName ) {
    return this.tableName = tableName == null ? "" : tableName;
  }

  /**
   * Returns the name of the parent table.
   *
   * @return A non-null string, possibly empty.
   */
  protected String getTableName() {
    return this.tableName;
  }

  /**
   * Returns the column name, if any. This should not be called
   * if the parser rule context doesn't know about columns.
   * 
   * @return A column instance, but could throw a class cast exception.
   */
  protected QueryParser.ColumnContext column() {
    return getColumnMapContext().column();
  }

  /**
   * Returns the table associated with the payload.
   *
   * @return The table from the table map context.
   */
  protected QueryParser.TableContext table() {
    return getTableMapContext().table(0);
  }

  /**
   * Casts the payload into the proper context, then extracts the column.
   *
   * @return The column associated with the column map parser rule.
   */
  protected QueryParser.ColumnMapContext getColumnMapContext() {
    return (QueryParser.ColumnMapContext)getParserRuleContext();
  }

  /**
   * Returns the nearest table context to this parser rule context.
   *
   * @return The wrapped parser rule context's table map context.
   */
  protected QueryParser.TableMapContext getTableMapContext() {
    return (QueryParser.TableMapContext)getParserRuleContext();
  }

  private void setParserRuleContext( ParserRuleContext ctx ) {
    this.parserRuleContext = ctx;
  }

  /**
   * Returns the payload used to associate the parser rule context with
   * a tree.
   *
   * @return The payload containing a parser rule context and tree.
   */
  protected ParserRuleContext getParserRuleContext() {
    return this.parserRuleContext;
  }

  protected void setNextContext( Context context ) {
    this.nextContext = context;
  }

  protected Context getNextContext() {
    return this.nextContext;
  }

  protected void setPrevContext( Context context ) {
    this.prevContext = context;
  }

  protected Context getPrevContext() {
    return this.prevContext;
  }

  /**
   * Creates a string of repeated text that is n characters long.
   *
   * @param n The number of characters in the resulting string.
   * @param ch The character to repeat.
   * @return A string padded with n characters.
   */
  public String repeat( int n, char ch ) {
    return CharBuffer.allocate( n ).toString().replace( '\0', ch );
  }

  public String toString() {
    return getParserRuleContext().getText();
  }
}

