package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.grammar.QueryParser;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Transforms <b><code>root &gt; element</code></b> into the starting
 * <code>SELECT</code> statement.
 */
public class RootContext extends Context {
  public RootContext( ParserRuleContext ctx ) {
    super( ctx );
  }

  /**
   * Transforms into SELECT, XMLROOT, and XMLELEMENT.
   *
   * @return "SELECT XMLROOT(" ... + ",XMLELEMENT(" ...
   */
  @Override
  public String getStart() {
    QueryParser.ElementContext element = getElementContext();
    String e = element.getText();

    return String.format( "XMLROOT(%s", startElement( e ) );
  }

  /**
   * Closes the XMLELEMENT and XMLROOT.
   */
  @Override
  public String getStop() {
    return String.format( ")%s)", getDeclaration() );
  }

  /**
   * Returns the value for the XML declaration.
   *
   * @return The XML declaration string in SQL/XML.
   */
  protected String getDeclaration() {
    return String.format( ",VERSION '%.1f',STANDALONE %s",
      getVersion(), isStandalone() ? "YES" : "NO" );
  }

  /**
   * Returns the XML version number, used for the <code>VERSION</code> value.
   *
   * @return 1.0f by default.
   */
  protected float getVersion() {
    return 1.0f;
  }

  /**
   * Indicates the value for <code>STANDALONE</code>.
   *
   * @return <code>true</code> STANDALONE is set <code>YES</code>;
   * <code>false</code> STANDALONE is set <code>NO</code>.
   */
  protected boolean isStandalone() {
    return true;
  }

  /**
   * Casts the payload into the proper context, then extracts the element.
   *
   * @return The element associated with the root parser rule.
   */
  protected QueryParser.ElementContext getElementContext() {
    return ((QueryParser.RootContext)getParserRuleContext()).element();
  }
}

