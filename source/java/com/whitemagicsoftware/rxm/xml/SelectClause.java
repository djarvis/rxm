package com.whitemagicsoftware.rxm.xml;

import com.whitemagicsoftware.rxm.Tree;
import com.whitemagicsoftware.rxm.grammar.QueryParser;

import org.antlr.v4.runtime.ParserRuleContext;

public class SelectClause extends Clause {
  private Tree<Context> tree;

  // The most recent column mapping path that does not contain ellipses.
  private QueryParser.ElementPathContext explicitPath;

  /**
   * Default constructor.
   */
  public SelectClause() {
  }

  /**
   * Returns the SELECT clause portion of the query.
   *
   * @return A non-null string starting with "SELECT".
   */
  public String toString() {
    return "SELECT" + getNewlineIndent() + super.toString();
  }

  /**
   * Adds the root context node to the top-most part of the tree. This
   * initializes the context tree to the root context node.
   *
   * @param ctx The payload to transform.
   */
  @Override
  public void enterRoot( QueryParser.RootContext ctx ) {
    setTree( createTree( new RootContext( ctx ) ) );
  }

  /**
   * Invoked when parsing <b><code>table &gt; path</code></b>. Adds a new
   * leaf to the context tree, then changes the context tree to the newly
   * added leaf.
   *
   * @param ctx The payload to transform.
   */
  @Override
  public void enterTableMap( QueryParser.TableMapContext ctx ) {
    setTree( addLeaf( new TableMapContext( ctx ) ) );
  }

  /**
   * Invoked when parsing <b><code>column &gt; path</code></b>. Adds a new
   * leaf to the context tree.
   *
   * @param ctx The payload to transform.
   */
  @Override
  public void enterColumnMap( QueryParser.ColumnMapContext ctx ) {
    ColumnMapContext cmc = new ColumnMapContext( ctx, getTableName() );

    if( cmc.hasEllipsesPath() ) {
      // A column mapping always has at least one non-ellipses mapping that
      // precedes it. (See directly below.)
      cmc.setExplicitPath( getExplicitPath() );
    }
    else {
      // A column mapping without ellipses must be stored for later use by
      // column mappings with ellipses.
      setExplicitPath( cmc.elementPath() );
    }

    addLeaf( cmc );
  }

  /**
   * Stores the latest column map's context that has no ellipses.
   *
   * @param epc The most recent element path that does not use ellipses.
   */
  protected void setExplicitPath( QueryParser.ElementPathContext epc ) {
    this.explicitPath = epc;
  }

  private QueryParser.ElementPathContext getExplicitPath() {
    return this.explicitPath;
  }

  /**
   * Invoked when parsing <b><code>column &gt; attribute</code></b>. Adds
   * a new leaf to the context tree.
   *
   * @param ctx The payload to transform.
   */
  @Override
  public void enterAttributeMap( QueryParser.AttributeMapContext ctx ) {
    addLeaf( new AttributeMapContext( ctx, getTableName() ) );
  }

  /**
   * Invoked when parsing <b><code>^</code></b>. Changes context tree to
   * its parent. If the parent doesn't exist, this will fail silently.
   *
   * @param ctx Indicates that context should switch, has no payload.
   */
  @Override
  public void enterPop( QueryParser.PopContext ctx ) {
    Tree<Context> parent = getTree().getParent();

    if( parent != null ) {
      setTree( parent );
    }
  }

  /**
   * Called after the last mapping statement is found. This builds the
   * SELECT statement using the tree created while walking the <b>rxm</b>
   * source.
   *
   * @param ctx Unused.
   */
  @Override
  public void exitStatements( QueryParser.StatementsContext ctx ) {
    append( getTree().getRoot().toString() );
  }

  /**
   * Adds the given parser rule context to the current context tree.
   *
   * @param ctx The parser rule context to add as a leaf to the context tree.
   * @return The leaf (tree) that was added.
   */
  private Tree<Context> addLeaf( Context ctx ) {
    return getTree().addLeaf( createTree( ctx ) );
  }

  /**
   * Changes where the next mapped items will be attached.
   */
  private void setTree( Tree<Context> tree ) {
    this.tree = tree;
  }

  /**
   * Returns where to attach upcoming map items.
   */
  private Tree<Context> getTree() {
    return this.tree;
  }

  /**
   * Creates a new tree with the given parser rule context.
   *
   * @param ctx The context to wrap in a payload that is added to a tree.
   * @return A new tree with a single data element and no branches.
   */
  private Tree<Context> createTree( Context ctx ) {
    return new SelectTree( ctx );
  }

  /**
   * Returns the table name used for column mappings.
   */
  private String getTableName() {
    return getTable().getText();
  }

  private QueryParser.TableContext getTable() {
    return ((QueryParser.TableMapContext)getParentParserRuleContext()).table(0);
  }

  private ParserRuleContext getParentParserRuleContext() {
    return getTreePayload().getParserRuleContext();
  }

  private Context getTreePayload() {
    return getTree().getPayload();
  }
}
