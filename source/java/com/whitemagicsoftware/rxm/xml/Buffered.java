package com.whitemagicsoftware.rxm.xml;

/**
 * Isolates creation of the buffer used to write the data.
 */
public interface Buffered {
  default StringBuilder createBuffer() {
    return new StringBuilder( 2048 );
  }
}

