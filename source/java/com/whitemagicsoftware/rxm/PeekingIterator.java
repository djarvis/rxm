package com.whitemagicsoftware.rxm;

import java.util.Iterator;

/**
 * Provides look-ahead abilities for an iterator.
 */
public class PeekingIterator<E> implements Iterator<E> {
  private final Iterator<? extends E> iterator;

  private boolean peekNext;
  private E elementNext;

  /**
   * Constructs an iterator with look-ahead abilities for a given iterator.
   *
   * @param iterator The iterator to adorn with look-ahead abilities.
   */
  public PeekingIterator( Iterator<? extends E> iterator ) {
    this.iterator = iterator;
  }

  /**
   * Answers whether there are more elements to iterate.
   *
   * @return true There is another element to iterate.
   */
  @Override
  public boolean hasNext() {
    return getPeekedNext() || getIterator().hasNext();
  }

  /**
   * Returns the next element in the list. If peek was called before
   * this method, then this method will return the peeked value.
   *
   * @return The next element.
   */
  @Override
  public E next() {
    return getPeekedNext() ? reset() : iteratorNext();
  }

  /**
   * Resets the peeked next state to false and sets the next peeked element
   * to null.
   *
   * @return The peeked next element.
   */
  private E reset() {
    E result = getElementNext();
    setPeekedNext( false );
    setElementNext( null );

    return result;
  }

  /**
   * Attempts to remove the currently iterated element. This will throw
   * an exception when trying to remove an element after peeking. This is
   * because next() has already been called on the underlying iterator.
   */
  @Override
  public void remove() {
    if( getPeekedNext() ) {
      throw new UnsupportedOperationException( "No removing after peeking." );
    }

    getIterator().remove();
  }

  /**
   * Returns the next element to iterate. Note that peeking prevents
   * removal.
   *
   * @return The next element in the iterator's list.
   */
  public E peek() {
    if( !getPeekedNext() ) {
      setElementNext( iteratorNext() );
      setPeekedNext( true );
    }

    return getElementNext();
  }

  private E iteratorNext() {
    return getIterator().next();
  }

  private E getElementNext() {
    return this.elementNext;
  }

  private void setElementNext( E elementNext ) {
    this.elementNext = elementNext;
  }

  private boolean getPeekedNext() {
    return this.peekNext;
  }

  private void setPeekedNext( boolean peekNext ) {
    this.peekNext = peekNext;
  }

  private Iterator<? extends E> getIterator() {
    return this.iterator;
  }
}

