package com.whitemagicsoftware.rxm;

import java.io.IOException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Tests the Relational eXpression Map.
 */
public class Main {
  /**
   * Reads the entire contents of the file (specified by the given path).
   * This uses the default character set when reading the file.
   *
   * @param path The path to the file with an <b>rxm</b> to read.
   * @throws IOException The file could not be read.
   */
  private static String readFile( String path ) throws IOException {
    Charset encoding = Charset.defaultCharset();
    byte[] encoded = Files.readAllBytes( Paths.get( path ) );
    return new String( encoded, encoding );
  }

  /**
   * Runs the parser to generate a SQL statement.
   *
   * @param args Contains the file name with an <b>rxm</b> to parse.
   *
   * @throws IOException The file could not be read.
   * @throws ParserException The file could not be parsed.
   */
  public static void main( String args[] )
    throws IOException, ParserException {
    RXM rxm = new RXM( readFile( args[0] ) );

    System.out.println( rxm.toSQL() );
  }
}
