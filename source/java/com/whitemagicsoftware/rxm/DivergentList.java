package com.whitemagicsoftware.rxm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Provides the ability to determine the index whereat two lists begin
 * to differ in content. Both this list and the list to compare against
 * must not contain null objects. Remove all nulls from both lists using
 * <code>list.removeAll(Collections.singleton(null))</code> (assuming
 * mutable lists).
 */
public class DivergentList<E> extends ArrayList<E> {
  @SuppressWarnings( "compatibility:-7553330276690450566" )
  private static final long serialVersionUID = 802724795120260337L;

  /**
   * Default (empty) constructor.
   */
  public DivergentList() {
  }

  /**
   * Constructs a list containing the elements of the
   * specified collection, in the order they are returned by the
   * collection's iterator.
   *
   * @param collection Container of elements to add to this list.
   */
  public DivergentList( Collection<? extends E> collection ) {
    super( collection );
  }

  /**
   * Determines the last common index that an object in this list is
   * the same as an object (at the same index) in the given list.
   *
   * @param list The list to compare against.
   *
   * @return -1 if the lists have no equal objects, or are empty.
   */
  public int diverges( DivergentList<E> list ) {
    int index = -1;

    // Determine the larger list for iterating.
    boolean smaller = list.size() < this.size();

    Iterator<E> iFew = (smaller ? list : this).iterator();

    // Iterate over the larger list.
    for( E e : (smaller ? this : list) ) {
      // Terminate when no more items exist in the smaller list.
      if( !(iFew.hasNext() && e.equals( iFew.next() )) ) {
        break;
      }

      index++;
    }

    return index;
  }
}
