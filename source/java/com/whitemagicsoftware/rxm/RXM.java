package com.whitemagicsoftware.rxm;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import com.whitemagicsoftware.rxm.xml.QueryBuilder;

/**
 * Abstracts SQL statement code into a terse mapping format.
 */
public class RXM {
  /** The SQL statement to execute. */
  private String query;

  /** List of known RXM modules. */
  private Map<String, String> modules;

  /**
   * Creates a new Relation eXpression Map (<b>rxm</b>). This will parse
   * content and update both the query and the modules as necessary.
   *
   * @param rxm The <b>rxm</b> to parse.
   * @throws ParserException The <b>rxm</b> could not be parsed.
   */
  public RXM( String rxm ) throws ParserException {
    QueryBuilder builder = new QueryBuilder( this );
    builder.parse( rxm );
  }

  /**
   * Walks through the <b>rxm</b> list and creates the equivalent
   * SQL statement.
   *
   * @return A SQL statement based on the current collection of modules.
   */
  public String toSQL() {
    return "";
  }

  /**
   * Returns an XML document based on the SQL statement created using
   * the <b>rxm</b> associated with this object.
   *
   * @param connection The database connection to use to generate the
   * resulting XML document.
   *
   * @return A non-null string.
   *
   * @throws SQLException The database connection could not be used to
   * generate a document.
   */
  public String toXML( Connection connection ) throws SQLException {
    return "";
  }

  /**
   * Returns a JSON document based on the SQL statement created using
   * the <b>rxm</b> associated with this object.
   *
   * @param connection The database connection to use to generate the
   * resulting JSON document.
   *
   * @return A non-null string.
   *
   * @throws SQLException The database connection could not be used to
   * generate a document.
   */
  public String toJSON( Connection connection ) throws SQLException {
    return "";
  }

  /**
   * Adds an <b>rxm</b> to the set of mappings.
   *
   * @param name The module name of the <b>rxm</b>.
   * @param rxm The<b>rxm</b> to associate with the given name.
   */
  public void addModule( String name, String rxm ) {
    getModules().put( name, rxm );
  }

  /**
   * Returns a collection of Relational eXpression Maps.
   *
   * @return An non-null map, possibly empty.
   */
  private Map<String, String> getModules() {
    Map<String, String> result = this.modules;

    if( result == null ) {
      setModules( result = createModules() );
    }

    return this.modules;
  }

  /**
   * Sets the container to hold the modules. Subclasses should override
   * createModules to introduce new behaviour.
   */
  private void setModules( Map<String, String> modules ) {
    if( modules == null ) {
      modules = createModules();
    }

    this.modules = modules;
  }

  /**
   * Returns the container to hold all modules.
   *
   * @return A non-null map by default.
   */
  protected Map<String, String> createModules() {
    return new HashMap<String, String>();
  }

  /**
   * Sets the SQL statement to execute when calling one of the toX()
   * methods.
   */
  private String getQuery() {
    return this.query;
  }

  /**
   * Stores the SQL statement after parsing the expression mappings.
   *
   * @param query The SQL statement to store
   */
  private void setQuery( String query ) {
    this.query = query;
  }
}

