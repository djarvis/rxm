package com.whitemagicsoftware.rxm;

import java.util.ArrayList;
import java.util.List;

/**
 * Stores a tree of values.
 * 
 * @param <T> The tree's playload.
 */
public class Tree<T> {
  /** Data associated with this leaf. */
  private T payload;

  /** Root of this tree, possibly null. */
  private Tree<T> parent;

  /** Child trees of this tree, possibly empty. */
  private List<Tree<T>> branches = new ArrayList<Tree<T>>();

  /**
   * Constructs a new tree, ready to have branches added.
   *
   * @param payload The data associated with this tree leaf (must not
   * be null).
   */
  public Tree( T payload ) {
    setPayload( payload );
  }

  /**
   * Adds the given tree to the list of this tree's branches. This will also
   * set the parent of the given tree to this tree instance.
   *
   * @param tree The tree to append to the list of branches.
   * @return The tree that was added (i.e., the tree parameter).
   */
  @SuppressWarnings( "unchecked" )
  public Tree<T> addLeaf( Tree<T> tree ) {
    getBranches().add( tree );
    tree.setParent( this );
    return tree;
  }

  /**
   * Finds the root to this tree by iterating over this tree's parent nodes.
   *
   * @return The root of this tree.
   */
  public Tree<T> getRoot() {
    Tree<T> root = this;

    // Traverse until there are no more parents, which should be the root.
    while( root.getParent() != null ) {
      root = root.getParent();
    }

    return root;
  }

  /**
   * Sets the parent tree to this tree.
   *
   * @param parent The root for this tree.
   */
  private void setParent( Tree<T> parent ) {
    this.parent = parent;
  }

  /**
   * Returns the parent tree to this tree, or null if this is the tree top.
   *
   * @return The root of this tree, or null if no more trees exist above.
   */
  public Tree<T> getParent() {
    return this.parent;
  }

  /**
   * Sets the data for this leaf in the tree.
   *
   * @param payload The data to associate with this leaf.
   */
  protected void setPayload( T payload ) {
    this.payload = payload;
  }

  /**
   * Returns the data held at this position in the tree.
   *
   * @return A non-null payload.
   */
  public T getPayload() {
    return this.payload;
  }

  /**
   * Answers whether the given payload equals the payload in this tree
   * instance.
   *
   * @param payload The payload to compare against.
   *
   * @return true The payload is the same.
   */
  public boolean hasPayload( T payload ) {
    return getPayload().equals( payload );
  }

  /**
   * Answers whether the given class is the same class as the payload for
   * this part of the tree.
   *
   * @param c The class to compare.
   * @return true The payloads are the same class.
   */
  public boolean isPayloadClass( Class c ) {
    return getPayload().getClass().equals( c );
  }

  /**
   * Returns the list of branches for this tree.
   *
   * @return A non-null, but possibly empty, list of branches.
   */
  public List<Tree<T>> getBranches() {
    return this.branches;
  }

  /**
   * Returns the location of the payload in the list of branches (sub-trees).
   * The index returned is 0-based.
   *
   * @param payload The payload to find within this tree's branches.
   * @return -1 if the payload doesn't exist.
   */
  public int indexOf( T payload ) {
    int index = 0;

    for( Tree<T> branch : getBranches() ) {
      if( branch.hasPayload( payload ) ) {
        return index;
      }

      // While the payload hasn't been found...
      index++;
    }

    // No payload found.
    return -1;
  }

  /**
   * Returns the first branch from this tree's list of branches.
   *
   * @return The first branch, possibly null.
   */
  public Tree<T> firstBranch() {
    return getBranches().get(0);
  }
}

