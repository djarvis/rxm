package com.whitemagicsoftware.rxm;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * Instances of this class allows multiple listeners to receive events
 * while walking the parse tree. For example:
 *
 * <pre>
 * ProxyParseTreeListener proxy = new ProxyParseTreeListener();
 * ParseTreeListener listener1 = ... ;
 * ParseTreeListener listener2 = ... ;
 * proxy.add( listener1 );
 * proxy.add( listener2 );
 * ParseTreeWalker.DEFAULT.walk( proxy, ctx );
 * </pre>
 */
public class ProxyParseTreeListener implements ParseTreeListener {
  private List<ParseTreeListener> listeners;

  /**
   * Creates a new proxy without an empty list of listeners. Add
   * listeners before walking the tree.
   */
  public ProxyParseTreeListener() {
    // Setting the listener to null automatically instantiates a new list.
    this( null );
  }

  /**
   * Creates a new proxy with the given list of listeners.
   *
   * @param listeners A list of listerners to receive events.
   */
  public ProxyParseTreeListener( List<ParseTreeListener> listeners ) {
    setListeners( listeners );
  }

  /**
   * Notifies the listeners and the context of an enter event.
   * 
   * @param ctx The rule context to receive an enter event.
   */
  @Override
  public void enterEveryRule( ParserRuleContext ctx ) {
    getListeners().forEach( listener -> {
      listener.enterEveryRule( ctx );
      ctx.enterRule( listener );
    });
  }

  /**
   * Notifies the listeners and the context of an exit event.
   * 
   * @param ctx The rule context to receive an exit event.
   */
  @Override
  public void exitEveryRule( ParserRuleContext ctx ) {
    getListeners().forEach( listener -> {
      ctx.exitRule( listener );
      listener.exitEveryRule( ctx );
    });
  }

  @Override
  public void visitErrorNode( ErrorNode node ) {
    getListeners().forEach( listener -> listener.visitErrorNode( node ) );
  }

  @Override
  public void visitTerminal( TerminalNode node ) {
    getListeners().forEach( listener -> listener.visitTerminal( node ) );
  }

  /**
   * Adds the given listener to the list of event notification recipients.
   *
   * @param listener A listener to begin receiving events.
   */
  public void add( ParseTreeListener listener ) {
    getListeners().add( listener );
  }

  /**
   * Removes the given listener to the list of event notification recipients.
   *
   * @param listener A listener to stop receiving events.
   * @return false The listener was not registered to receive events.
   */
  public boolean remove( ParseTreeListener listener ) {
    return getListeners().remove( listener );
  }

  /**
   * Removes all event notification recipients.
   */
  public void clear() {
    getListeners().clear();
  }

  /**
   * Returns the list of listeners.
   *
   * @return The list of listeners to receive tree walking events.
   */
  private List<ParseTreeListener> getListeners() {
    return this.listeners;
  }

  /**
   * Changes the list of listeners to receive events. If the given list of
   * listeners is null, an empty list will be created.
   *
   * @param listeners A list of listeners to receive tree walking
   * events.
   */
  public void setListeners( List<ParseTreeListener> listeners ) {
    this.listeners = listeners == null ?
      createParseTreeListenerList() : listeners;
  }

  /**
   * Creates a CopyOnWriteArrayList to permit concurrent mutative
   * operations.
   *
   * @return A thread-safe, mutable list of event listeners.
   */
  protected List<ParseTreeListener> createParseTreeListenerList() {
    return new CopyOnWriteArrayList<ParseTreeListener>();
  }
}
