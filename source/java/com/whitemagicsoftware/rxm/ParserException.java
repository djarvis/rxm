package com.whitemagicsoftware.rxm;

/**
 * Indicates a problem happened while parsing.
 */
public class ParserException extends RuntimeException {
  @SuppressWarnings( "compatibility:-809134579270307487" )

  private static final long serialVersionUID = -2378827486768219586L;

  /** The line number in the <b>rxm</b> that could not be parsed. */
  private int lineNumber;

  /** The <b>rxm</b> context wherein the parsing failed. */
  private String context;

  /**
   * Creates a new exception that indicates a problem parsing an <b>rxm</b>
   * string.
   *
   * @param lineNumber The problematic line in the <b>rxm</b>.
   * @param context The problematic code in the <b>rxm</b>.
   * @param message The problem with the <b>rxm</b>.
   */
  public ParserException( int lineNumber, String context, String message ) {
    super( message );
    setLineNumber( lineNumber );
    setContext( context );
  }

  private void setLineNumber( int lineNumber ) {
    this.lineNumber = lineNumber;
  }

  private void setContext( String context ) {
    this.context = context;
  }

  private int getLineNumber() {
    return this.lineNumber;
  }

  private String getContext() {
    return this.context;
  }

  /**
   * Returns a message that denotes where the parsing problem was encountered.
   *
   * @return A non-null text string suitable for displaying to developers.
   */
  public String toString() {
    return String.format( "Line: %s; near: \"%s\"; error: %s\n",
      getLineNumber(), getContext(), getMessage() );
  }
}

