/* Relational eXpression Map (rxm) */
grammar Query ;

/************************************************************************
 *
 * Define lexer rules.
 *
 ************************************************************************/

/* Define the keywords first (otherwise T_ID would match first). */
T_ROOT  : 'root' ;
T_IMPORT: 'import' ;
T_MODULE: 'module' ;
T_NULL  : 'null' ;

/* Define SQL strings that can include space tokens. */
T_STRING: '\'' (~'\'' | '\'\'')* '\'' ;

/* Define skippable tokens. */
T_WS: [ \t\r\n]+ -> skip ;

/* Define digits and numbers. */
T_DIGIT: [0-9] ;
T_SIGN : '+' | '-' ;

T_FLOAT   : T_SIGN? (T_NUMBER | T_FRACTION) ;
T_NUMBER  : T_DIGIT | T_DIGIT T_NUMBER ;
T_FRACTION: T_NUMBER? '.' T_NUMBER ;

/* Define map symbols; exclude '>', which is defined as T_GT. */
T_POP  : '^' ;
T_GLOB : '.*' ;
T_INNER: '+>' ;
T_OUTER: '->' ;
T_COMMA: ',' ;
T_WHERE: ';' ;

/* Define table and path symbols. */
T_ELLIPSES: '...' ;
T_AT      : '@' ;
T_SLASH   : '/' ;
T_PERIOD  : '.' ;

/* Define an identifier. */
T_ID: [A-Za-z0-9_]+ ;

/* Define WHERE clause comparitor operator tokens. */
T_EQ   : '=' ;
T_INEQ : '<>' ;
T_LT   : '<' ;
T_GT   : '>' ;
T_LTE  : '<=' ;
T_GTE  : '>=' ;

/* Define the WHERE clause logical operator tokens. */
T_LOGIC_AND : '&&' ;
T_LOGIC_OR  : '||' ;

/* Define the WHERE clause logical grouping tokens. */
T_EXPR_OPEN : '(' ;
T_EXPR_CLOSE: ')' ;

/* Define the WHERE clause set grouping tokens. */
T_EXPR_SET_OPEN : '{' ;
T_EXPR_SET_CLOSE: '}' ;

/* Define the WHERE clause parameter token identifier prefix. */
T_EXPR_PARAMETER: ':' ;

/************************************************************************
 *
 * Define SELECT and JOIN statement.
 *
 ************************************************************************/

query: start statements where? EOF ;

/* Define the root, line map, and JOIN clauses. */
start : (root | module) T_COMMA ;
root  : T_ROOT T_GT element ;
module: T_MODULE T_ID ;

statements: statement+ ;
statement : (pop | glob | map | include) T_COMMA ;
pop       : T_POP ;
glob      : T_GLOB ;

/* Map lines affect the tree depth differently. */
map         : tableMap | columnMap | attributeMap | joinMap ;
tableMap    : table T_GT table ;
columnMap   : column T_GT path ;
attributeMap: column T_GT attribute ;
joinMap     : tableColumn (T_INNER | T_OUTER) tableColumn ;

include     : T_IMPORT T_ID ;

table       : T_ID ;
column      : T_PERIOD T_ID ;
tableColumn : table column ;

attribute   : T_AT T_ID ;

path        : elementPath | ellipsesPath;
element     : T_ID ;
elementPath : element (T_SLASH element)* ;
ellipsesPath: T_ELLIPSES T_SLASH elementPath ;

/************************************************************************
 *
 * Define WHERE clause.
 *
 ************************************************************************/

where         : T_WHERE expression? ;

expression    : exprEquality | exprLogicalAnd ;
exprLogicalAnd: exprLogicalOr (exprAnd exprLogicalOr)* ;
exprLogicalOr : exprRelational (exprOr exprRelational)* ;
exprRelational: exprEquality | exprCompRel ;
exprEquality  : exprParen | exprCompEqual ;
exprParen     : T_EXPR_OPEN expression T_EXPR_CLOSE ;

exprAnd       : T_LOGIC_AND ;
exprOr        : T_LOGIC_OR ;

exprCompRel   : tableColumn (T_LT | T_GT | T_LTE | T_GTE) exprValue ;
exprCompEqual : tableColumn (T_EQ | T_INEQ) (exprValue | exprSet) ;

exprValue     : exprParameter | literal ;

exprParameter : T_EXPR_PARAMETER T_ID ;
literal       : T_NULL | T_STRING | T_FLOAT ;

exprSet       : T_EXPR_SET_OPEN exprList T_EXPR_SET_CLOSE ;
exprList      : exprValue (T_COMMA exprValue)* ;

